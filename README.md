# QUORUM NETWORWS & CONTRACT DEPLOYMENT

> Autor: Franco Geller (franco.geller@gmail.com)

* Analisis de redes de blockchain Quorum
    1. ibft (Ver mas en ibft)
    2. raft (Ver mas en raft)

* Automatizacion de despliegues con docker
    1. ibft
        * Se automatizo el despliegue centralizado de N nodos por contenedor.
    2. raft
        * Se automatizo el despliegue distribuido de nodos tanto raiz como selladores en multiples contenedores.

* Despliegue de contratos
    1. Se despliega un contrato de ejemplo y se explica como manipularlo (Ver mas en contracts).
    2. Se automatiza el despliegue de contratos a partir de un JSON estandarizado (Generado con openzeppelin) (Ver mas en contracts-openzeppelin).