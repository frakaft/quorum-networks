// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

import "@openzeppelin/contracts/presets/ERC1155PresetMinterPauser.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract PruebasToken is ERC1155PresetMinterPauser, Ownable {

    uint256 public constant PPA = 0;
    uint256 public constant PPS = 1;
    uint256 public constant DBI = 2;

    event NewURI(address, string newuri);

    constructor()  public ERC1155PresetMinterPauser("") {
        _mint(msg.sender, PPA, 10**18, "");
        _mint(msg.sender, PPS, 10**18, "");
        _mint(msg.sender, DBI, 10**18, "");
    }

    function setURI(string memory newuri) public onlyOwner {
        _setURI(newuri);
        emit NewURI(msg.sender, newuri);
    }
}