# DESPLIEGUE DE CONTRATO DE OPENZEPPELIN

1. Situacion

    * Al contruir el contrato con openzeppelin, se genera un JSON estandarizado con mucha metadata del contrato.
    * Para poder desplegar este contrato manualmente en nuestro cliente geth, debemos extraer de este JSON el bytecode y la abi.

2. Analisis del JSON

    * El JSON que contiene todos los datos necesarios para desplegar el contrato contiene las siguiente estructura:
        
        ```
        {
            "fileName": "...",
            "contractName": "...",
            "source": "...",
            "sourcePath": "...",
            "sourceMap": "...",
            "deployedSourceMap": "...",
            "abi": [...],
            "ast": {...},
            "bytecode": "...",
            "deployedBytecode": "...",
            "compiler": {...},
            "networks": {...}
        }
        ```
        
    * De este JSON, debemos extrar el arreglo de la clave "abi" y el string de la clave "bytecode".

    > IMPORTANTE: Al extraer el contenido de la calve bytecode, remover el "0x" inicial del string.
    > Ver los archivos PruebasToken.bin, PruebasToken.abi y PruebasToken.js

3. Generar JavaScript de despliegue

    * Para desplegar el contrato, podemos hacer el procedimiento manualmente, entrando a la consola geth y paso a paso ejecutar los distintos comandos para desplegarlo. (Ver ejemplos en carpeta "contracts")
    * Una opcion mas rapida es generar un JavaScript con el paso a paso, y luego desplegar ese JS desde la consola geth (Ver archivo PruebasToken.js). Tener en cuenta que este archivo realiza un unlock de la wallet del nodo. Modificar el passphrase de la wallet en caso de ser distinta de "" (string vacio).

4. Desplegar el JavaScript

    * Para desplegar nuestro archivo de despluegue debemos:

        1. Copiamos el archivo PruebasToken.js al directorio raiz de la red (Por defecto: /Raft-Network/).
        2. Parados en el directorio raiz de la red, donde se encuentra el archivo PruebasToken.js. Ingresamos a la consola geth del nodo raiz:

            ```
            geth attach Node-0/data/geth.ipc
            ```
        
        3. Ejecutamos el contrato (esto puede demorar unos segundos):
        
            ```
            loadScript("PruebasToken.js")
            ```
        
        4. Este script, creara varias variables. La mas importante es la variable contract que contiente el address del contrato:
        
            ```
            contract.address
            ```
        
        5. Si al consolear el address, este devuelve un valor distinto de null, significa que el contrato ha sido desplegado.