# IBFT NETWORK

> https://docs.goquorum.consensys.net/en/stable/Tutorials/Create-IBFT-Network/

1. Dependencias

    ```
    git clone https://github.com/Consensys/quorum.git
    ```
    ```
    git clone https://github.com/ConsenSys/istanbul-tools.git
    ```
    ```
    wget -c https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz
    ```

2. Build

    ```
    docker build -t ibft .
    ```

3. Environment variables default values

    ```
    BC_ROOT=/IBFT-Network   ->  Path ROOT de la red, se recomienda no cambiar
                                Se debe montar un volumen apuntado a este path
                                default:/IBFT-Network

    NODES=5                 ->  Cantidad de nodos que se desplegaran en el contenedor
                                Debe ser mayor a cero
                                default:5

    HOST_IP=127.0.0.1       ->  IP publica del contenedor
                                deafult:127.0.0.1

    AUTO_START=no|yes       ->  Define si los nodos se auto levantan y se agregan al cluster al lanzar el contenedor
                                default:yes

    FORCE_CLEAN=no|yes      ->  Borrar todo el path root de la red al lanzar el contenedor
                                En caso de forzar el borrado, la red se auto configurara de cero
                                default:no
    ```

4. Run

    * Auto start

        ```
        docker run \
        -e HOST_IP=127.0.0.1 \
        -e NODES=5 \
        -p 22000:22000 -p 22001:22001 -p 22002:22002 -p 22003:22003 -p 22004:22004 \
        -p 30300:30300 -p 30301:30301 -p 30302:30302 -p 30303:30303 -p 30304:30304 \
        -v /opt/IBFT-Network:/IBFT-Network \
        --name ibft -d ibft
        ```

    * Manual start (attached console)

        ```
        docker run \
        -e HOST_IP=127.0.0.1 \
        -e NODES=5 \
        -p 22000:22000 -p 22001:22001 -p 22002:22002 -p 22003:22003 -p 22004:22004 \
        -p 30300:30300 -p 30301:30301 -p 30302:30302 -p 30303:30303 -p 30304:30304 \
        -v /opt/IBFT-Network:/IBFT-Network \
        --name ibft -it ibft bash
        ```

        > Inicializar manualmente los nodos
        > Definir $NODE_DATA, $NODE_RPC_PORT, $NODE_PORT & $NODE_LOG

        ```
        nohup env PRIVATE_CONFIG=ignore geth --datadir $NODE_DATA --nodiscover --istanbul.blockperiod 5 --syncmode full --mine --minerthreads 1 --verbosity 5 --networkid 10 --rpc --rpcaddr 127.0.0.1 --rpcport $NODE_RPC_PORT --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul --emitcheckpoints --port $NODE_PORT &> "$NODE_LOG" &
        ```

    ## Geth
    
    * Dentro del contenedor
    
        ```
        geth atach /IBFT-Network/Node-0/data/geth.ipc
        ```
        ```
        eth.blockNumber
        ```
        ```
        istanbul.getValidators("latest")
        ```