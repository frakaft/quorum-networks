#!/usr/bin/env bash

# default variables
DEFAULT_BC_ROOT='/IBFT-Network'
DEFAULT_NODES=5
DEFAULT_NODE_IP=127.0.0.1
DEFAULT_AUTO_START=yes
DEFAULT_FORCE_CLEAN=no

# check to see if this file is being run or sourced from another script
_is_sourced() {
    # https://unix.stackexchange.com/a/215279
    [ "${#FUNCNAME[@]}" -ge 2 ] \
    && [ "${FUNCNAME[0]}" = '_is_sourced' ] \
    && [ "${FUNCNAME[1]}" = 'source' ]
}

# init BC_ROOT if unset
check_for_bc_root() {
    if [ -z "$BC_ROOT" ]
    then
        BC_ROOT=$DEFAULT_BC_ROOT
    fi
}

# init FORCE_CLEAN if unset
check_for_force_clean() {
    if [ -z "$FORCE_CLEAN" ]
    then
        FORCE_CLEAN=$DEFAULT_FORCE_CLEAN
    fi
}

# init NODE_IP if unset
check_for_NODE_IP() {
    if [ -z "$NODE_IP" ]
    then
        NODE_IP=$DEFAULT_NODE_IP
    fi
}

# init NODES if unset
check_for_nodes() {
    if [ -z "$NODES" ]
    then
        NODES=$DEFAULT_NODES
    fi
}

# init AUTO_START if unset
check_for_auto_start() {
    if [ -z "$AUTO_START" ]
    then
        AUTO_START=$DEFAULT_AUTO_START
    fi
}

# init static nodes file
init_static_nodes_file() {
    sed -i "s/0.0.0.0/$NODE_IP/g" $BC_ROOT/static-nodes.json
    sed -i "s/30303/NODE_PORT/g" $BC_ROOT/static-nodes.json
    
    local NODE=0
    while [ $NODE -lt $NODES ]; 
    do
        NODE_PORT=$(printf 303"%02d" $NODE)
        echo $NODE_PORT
        sed -i "0,/NODE_PORT/s//$NODE_PORT/" $BC_ROOT/static-nodes.json 
        NODE=$(($NODE+1))
    done
}

# init each node
init_nodes() {
    local NODE=0
    while [ $NODE -lt $NODES ]; 
    do
        NODE_DATA=$BC_ROOT/Node-$NODE/data
        mkdir -p $NODE_DATA
        cp $BC_ROOT/static-nodes.json $NODE_DATA
        cp $BC_ROOT/$NODE/nodekey $NODE_DATA
        geth --datadir $NODE_DATA init $BC_ROOT/genesis.json
        NODE=$(($NODE+1))
    done
}

# start each node
start_nodes() {
    local NODE=0
    while [ $NODE -lt $NODES ]; 
    do
        NODE_DATA=$BC_ROOT/Node-$NODE/data
        NODE_PORT=$(printf 303"%02d" $NODE)
        NODE_RPC_PORT=$(printf 220"%02d" $NODE)
        NODE_LOG=$BC_ROOT/Node-$NODE/data/node.log
        nohup env PRIVATE_CONFIG=ignore geth --allow-insecure-unlock --datadir $NODE_DATA --nodiscover --istanbul.blockperiod 5 --syncmode full --mine --minerthreads 1 --verbosity 5 --networkid 10 --rpc --rpcaddr 127.0.0.1 --rpcport $NODE_RPC_PORT --rpcapi admin,db,eth,debug,miner,net,shh,txpool,personal,web3,quorum,istanbul --emitcheckpoints --port $NODE_PORT &> "$NODE_LOG" &
        NODE=$(($NODE+1))
    done
}

# clean root
clean_data() {
    rm -rf $BC_ROOT/*
}

_main() {
    check_for_bc_root
    check_for_NODE_IP
    check_for_nodes
    check_for_auto_start
    check_for_force_clean

    if [ "$FORCE_CLEAN" == "yes" ]; then
        clean_data
    fi

    local GENESIS_FILE=$BC_ROOT/genesis.json
    if [ -f "$GENESIS_FILE" ]; then
        echo "$GENESIS_FILE exists."
    else
        clean_data
        istanbul setup --num $NODES --nodes --quorum --save --verbose
        init_static_nodes_file
        init_nodes
    fi

    if [ "$AUTO_START" == "yes" ]; then
        start_nodes
    fi

    if [ "$AUTO_START" == "yes" ] && [ "$@" == "tail_main" ]; then
        tail -f $BC_ROOT/Node-0/data/node.log
    fi

    if [ "$@" == "tail_main" ]; then
        exec "bash"
    else
        exec "$@"
    fi
}

if ! _is_sourced; then
    _main "$@"
fi
