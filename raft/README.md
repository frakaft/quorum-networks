# RAFT NETWORK

> https://docs.goquorum.consensys.net/en/stable/Tutorials/Create-a-Raft-network/

1. Dependencias

    ```
    git clone https://github.com/Consensys/quorum.git
    ```
    ```
    wget -c https://dl.google.com/go/go1.14.2.linux-amd64.tar.gz
    ```

2. Build

    ```
    docker build -t raft .
    ```

3. Environment variables

    ```
    BC_ROOT=/Raft-Network   ->  Path ROOT de la red, se recomienda no cambiar
                                Se debe montar un volumen apuntado a este path
                                default:/Raft-Network

    NODES=5                 ->  Cantidad de nodos que se desplegaran en el contenedor
                                Debe ser mayor a cero
                                default:5

    HOST_IP=127.0.0.1       ->  IP publica del contenedor
                                deafult:127.0.0.1

    AUTO_START=no|yes       ->  Define si los nodos se auto levantan y se agregan al cluster al lanzar el contenedor
                                default:yes

    FORCE_CLEAN=no|yes      ->  Borrar todo el path root de la red al lanzar el contenedor
                                En caso de forzar el borrado, la red se auto configurara de cero
                                default:no

    PASSWORD=''             ->  Passphrase por defecto de todas las wallets de todos los nodos de este contenedor
                                Se utiliza al ejecutar personal.unlockAccount(<account>)
                                default:''
    ```

4. Run

    * Desplegar contenedor con nodo raiz con sub nodos

    > Al desplegar el contenedor con el nodo raiz, aprovechamos para desplegar varios nodos junto a el.
    > La wallet del nodo raiz dentro de este contenedor (Node-0), es la que tendra todo el ether existente en la red.
    > Aunque esto solo nos servira para hacer transacciones de balance, ya que las transacciones en esta red no consumen gas.
    > Se devine a travez de variables de entorno, la IP publica del contenedor, la cantidad de nodos a deplegar, y los puertos a enlazar para los respectivos nodos.

        ```
        docker run \
        -e HOST_IP=127.0.0.1 \
        -e NODES=5 \
        -p 21000:21000 -p 21001:21001 -p 21002:21002 -p 21003:21003 -p 21004:21004 \
        -p 22000:22000 -p 22001:22001 -p 22002:22002 -p 22003:22003 -p 22004:22004 \
        -p 50000:50000 -p 50001:50001 -p 50002:50002 -p 50003:50003 -p 50004:50004 \
        -v /Raft-Network:/Raft-Network \
        --name raft-root -d raft
        ```

    > En este contenedor se encuentra el nodo raiz (Node-0) Para acceder a la consola geth de la raiz:

        ```
        docker exec -it raft-root geth attach /Raft-Network/Node-0/data/geth.ipc
        ```

    > En este contenedor, se genera el genesis.json de la red (Por defecto: /Raft-Network/genesis.json). Este JSON no debe ser modificado nunca.y continene toda la informacion para que un nuevo nodo pueda ser agregado a la red.
    
    * Desplegar contenedor con nuevos nodos 
    
    > Agregar nodos la red red existenete es un proceso sincroinizado
    > Debemos tener acceso al Nodo Raiz para poder 

        ```
        docker run \
        -e HOST_IP=127.0.0.1 \
        -e NODES=5 \
        -p 21000:21000 -p 21001:21001 -p 21002:21002 -p 21003:21003 -p 21004:21004 \
        -p 22000:22000 -p 22001:22001 -p 22002:22002 -p 22003:22003 -p 22004:22004 \
        -p 50000:50000 -p 50001:50001 -p 50002:50002 -p 50003:50003 -p 50004:50004 \
        -v /Raft-Network:/Raft-Network  \
        -v /genesis.json:/docker-entrypoint-init/genesis.json  \
        --detach-keys="ctrl-a" \
        --name raft-peer -it raft
        ```

    > Una vez configurados todos los nodos, CTRL+A para cerrar el contenedor sin finalizarlo

    ## Geth

    * Dentro de un contenedor

        ```
        geth atach /Raft-Network/Node-0/data/geth.ipc
        ```

    * Entendiendo el cluster raft

        ```
        raft.cluster
        ```

    * El cluster raft tiene 3 roles

        * minter -> Es el lider de la red, no es necesariamente el nodo raiz, y este puede ir rotando entre los verificadores
        * verifier -> Es un verificador de la red. Para agregar un verificador, en el nodo raiz ejecutar:
            
            ```
            raft.addPeer('enode://<EnodeID>@<EnodeIP>:2100*?discport=0&raftport=5000*')
            ```

        * learner -> Es un aprendiz de la red. Para agregar un aprendiz, en el nodo raiz ejecutar:
            
            ```
            raft.addLearner('enode://<EnodeID>@<EnodeIP>:2100*?discport=0&raftport=5000*')
            ```

    * Otros comandos utiles

        * Promover un aprendiz a verificador
            
            ```
            raft.promoteToPeer(<raftId>)
            ```

        * Borrar un verificador
            
            ```
            raft.removePeer(<raftId>)
            ```
            