pragma solidity ^0.6.0;

contract Storage {
    string _secret;
    uint _count;
    address _owner;
    address _last;

    constructor() public {
        _secret = "";
        _count = 0;
        _owner = msg.sender;
        _last = msg.sender;
    }

    function tell(string memory secret) public {
        _secret = secret;
        _count = _count + 1;
        _last = msg.sender;
    }

    function secret() public view returns (string memory) {
        return _secret;
    }

    function count() public view returns (uint) {
        return _count;
    }

    function owner() public view returns (address) {
        return _owner;
    }

    function last() public view returns (address) {
        return _last;
    }
}