# DESPLIEGUE DE CONTRATOS CON GETH

1. Dependencias

    ```
    npm install -g solc@0.6.0
    ```
    ```
    solcjs --version
    ```

2. Generar ABI y BYTECODE

    ```
    solcjs ContractStorage.sol --optimize --abi --bin
    ```

 *  Esto generara la ABI y el Bytecode del contrato:

    1. ContractStorage_sol_Storage.abi
    2. ContractStorage_sol_Storage.bin

3. Desplegar contrato en consola geth

    ```
    geth attach /Raft-Network/Node-0/data/geth.ipc
    ```

    * Dentro de la consola geth

        ```
        var abi = ´Contenido de ContractStorage_sol_Storage.abi´;
        var bytecode = "´Contenido de ContractStorage_sol_Storage.bin´";
        var contractTemplate = web3.eth.contract(abi);
        var contractGas = eth.estimateGas({ data: "0x" + bytecode }) * 2;
        // en caso de fallar var contractGas = 0xffffffffffffff; pero no abusar de esto
        // considerar optimizar el bytecode
        // tener en cuanta que el gas estimado tiene una chance del 50% de estar mal
        personal.unlockAccount(eth.accounts[0], "");
        var contract = contractTemplate.new({from: web3.eth.accounts[0], data: "0x" + bytecode, gas: contractGas }, function(e, contract){ console.log( e ? e : contract); });
        console.log(contract.address);
        ```

4. Interaccion con el contrato (dentro de la consola geth)

    * Instanciar el contrato

        ```
        var contractInstance = eth.contract(abi).at(contract.address);
        ```

    * Metodos del contrato

        1. tell(secret: string): void   -> Guarda un secreto en el contrato (escritura, genera transaccion)

            ```
            contractInstance.tell("mi secreto", { from: eth.accounts[0] });
            ```

        2. secret(): string            -> Devuelve el ultimo secreto guardado en el contrato (solo lectura, no genera transacciones)
            
            ```
            contractInstance.secret.call()
            ```

        3. count(): uint             -> Cantidad de secretos divulgados (solo lectura, no genera transacciones)
            
            ```
            contractInstance.count.call()
            ```

        4. owner(): address          -> Creador del contrato (solo lectura, no genera transacciones)
            
            ```
            contractInstance.owner.call()
            
            ```
        5. last(): address           -> Ultimo address en contar un secreto (solo lectura, no genera transacciones)
            
            ```
            contractInstance.last.call()
            ```

4. Comandos utiles
    
    ```
    txpool.inspect -> Vemos el estado de las transacciones en cola
    ```
